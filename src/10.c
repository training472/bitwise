unsigned int setbits(unsigned int x, unsigned int p, unsigned int n, unsigned int y)
{
	unsigned int op1 = (y & (~(~0 << n ))) << ( p - n + 1) ; 
	unsigned int op2 = (x & ~(~(~0 << n ) << ( p - n + 1)));
	x = op1 | op2;
	
	return x;
}
