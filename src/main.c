#include <stdio.h>
#include <stdlib.h>
#include "../include/showbits.h"
#include "../include/header.h"

int main(void)
{
	int snum;
	int dnum;
	int n;
	int s;
	int d;
	int num;
	int pos;
	int p;
	int x;
	int y;
	int ch;
	int temp;

	do{	
		printf("---------------------------\n");
		printf("Select a problem statement:\n");
		printf("---------------------------\n");
		printf("1. Given 2 bit positions d and s in a number n, swap the bit values between s and d.\n");
		printf("2. Given the bit position s in number snum and the bit position d in number dnum, swap bit values between s and d. \n");
		printf("3. Function bit_copy (int snum, int dnum, int n, int s, int d) which copies n bits (right adjusted) from bit position s in snum to bit position d in dnum. \n");
		printf("4. Functions which will toggle even bits and odd bits respectively in a given number \n");
		printf("5. Macro to test and set a bit position in a number. \n");
		printf("6. Functions for\n\ta. unsigned int left_rotate_bits (unsigned int num);\n\tb. unsigned int right_rotate_bits (unsigned int num);\n\tc. unsigned int left_rotate_n_bits (unsigned int num, int n);\n\td. unsigned int right_rotate_n_bits (unsigned int num, int n);\n");
		printf("7. Functions which will count the number of bits set and number of bits cleared in the given number num respectively \n");
		printf("8. Functions for \n\ta. int cnt_leading_set_bits (int num)\n\tb. int cnt_leading_cleared_bits (int num)\n\tc. int cnt_trailing_cleared_bits (int num)\n\td. int cnt_trailing_set_bits (int num)\n");
		printf("9. Macros for the following using bitwise operations: \n\ta. To find maximum and minimum of 2 numbers\n\tb. To clear right most set bit in a number\n\tc. To clear left most set bit in a number\n\td. To set right most cleared bit in a number\n\te. To set left most cleared bit in a number\n\tf. To set bits from bit position ‘s’ to bit position ‘d’ in a given number and clear rest of the bits\n\tg. To clear bits from bit position ‘s’ to bit position ‘d’ in a given number and set rest of the bits\n\th. To toggle bits from bit position ‘s’ to bit position ‘d’ in a given number\n");
		printf("10. Function setbits (x,p,n,y) that returns x with the ‘n’ bits that begin at position p set to the right most n bits of y, leaving the other bits unchanged.\n");
		printf("11. Function invert (x,p,n) that returns x with n bits that begin at position p inverted, leaving others unchanged.\n");
		printf("12. Macro getbits (x,p,n) that retuns (left adjusted) n-bit field of x that begins at position p.\n");
		printf("\n\nPRESS 0 TO EXIT\n\n");
		printf("---------------------------\n");
		printf("Enter your choice: ");
		scanf("%d",&ch);

		switch (ch) {
			case 1: printf("\nEnter the number: ");
				scanf("%d", &num);	
				printf("\nEnter the value of s: ");
				scanf("%d", &s);
				printf("\nEnter the value of d: ");
				scanf("%d", &d);
				printf("\n");
				
				printf("Num: %d -> ", num);
        			showbits(num);
       		 		printf("\n");

				temp = swap_bits(num, s, d);

				printf("Bits after swap: %d -> ", temp);
        			showbits(temp);
        			printf("\n\n");

				break;
			
			case 2: printf("\nEnter the numbers: ");
				scanf("%d%d", &snum, &dnum);
				printf("\nEnter the value of s: ");
				scanf("%d", &s);
				printf("\nEnter the value of d: ");
				scanf("%d", &d);
				printf("\n");

				printf("\nsnum before modification: %d -> ", snum);
        			showbits(snum);

        			printf("\ndnum before modification: %d -> ", dnum);
        			showbits(dnum);

        			swap_bits_between(&snum, &dnum, s, d);

        			printf("\nsnum after modification: %d -> ", snum);
        			showbits(snum);

        			printf("\ndnum after modification: %d -> ", dnum);
        			showbits(dnum);
				printf("\n\n");
				
				break;

			case 3: printf("\nEnter the numbers: ");
                                scanf("%d%d", &snum, &dnum);
                                printf("\nEnter the value of s: ");
                                scanf("%d", &s);
                                printf("\nEnter the value of n: ");
                                scanf("%d", &n);
				printf("\nEnter the value of d: ");
				scanf("%d", &d);
                                printf("\n");

                                printf("\nsnum before modification: %d -> ", snum);
                                showbits(snum);

                                printf("\ndnum before modification: %d -> ", dnum);
                                showbits(dnum);
				
				temp = copy_bits(snum, dnum, s, n, d);

				printf("\nsnum after modification: %d -> ", snum);
                                showbits(snum);

                                printf("\ndnum after modification: %d -> ", temp);
                                showbits(temp);
                                printf("\n\n");

				break;

			case 4: printf("\nEnter the number: ");
				scanf("%d", &num);
				printf("\nNumber before toggle: ");
        			showbits(num);
        			printf("\n");

        			int e_num = even_bit_toggle(num);

        			printf("\nNumber after even-bit toggle: ");
        			showbits(e_num);
        			printf("\n");

        			int o_num = odd_bit_toggle(num);

        			printf("\nNumber after odd-bit toggle: ");
        			showbits(o_num);
        			printf("\n\n");

				break;

			case 5: printf("\nEnter the number: ");
				scanf("%d", &num);
				printf("\nEnter the position: ");
				scanf("%d", &pos);
				
				int ts_num = bit_ts(num, pos);

        			printf("\nNumber: ");
        			showbits(num);
        			printf("\n");

        			printf("\nNumber after bit-set: ");
        			showbits(ts_num);
        			printf("\n");

				break;

			case 6: printf("\nEnter the number: ");
                                scanf("%d", &num);
                                printf("\nEnter the value of n: ");
                                scanf("%d", &n);
				
				printf("\nNum: %d -> ", num);
        			showbits(num);
        			printf("\n");

        			int l_num = left_rotate_bits(num);
        			printf("Number After left rotate: %d -> ", l_num);
        			showbits(l_num);
        			printf("\n");

        			int r_num = right_rotate_bits(num);
        			printf("Number after right rotate: %d -> ", r_num);
        			showbits(r_num);
        			printf("\n");

        			int l_n_num = left_rotate_n_bits(num, n);
        			printf("Number after n bits left rotate: %d ->", l_n_num);
        			showbits(l_n_num);
        			printf("\n");

        			int r_n_num = right_rotate_n_bits(num, n);
        			printf("Number after n bits right rotate: %d -> ", r_n_num);
        			showbits(r_n_num);
        			printf("\n");

				break;

			case 7: printf("\nEnter the number: ");
				scanf("%d", &num);
				
				printf("\nNum: %d -> ", num);
                                showbits(num);
                                printf("\n");
				
				printf("\nNumber of set bits: %d", count_bit_set(num));

				printf("\nNumber of clear bits: %d", count_bit_clear(num));
				
				printf("\n\n");

				break;
				
			case 8: printf("\nEnter the number: ");
                                scanf("%d", &num);

				printf("\nNum: %d -> ", num);
                                showbits(num);
                                printf("\n");
				
				printf("\nNumber of leading set bits: %d \n", cnt_leading_set_bits(num));
        			printf("\nNumber of leading clear bits: %d \n", cnt_leading_clear_bits(num));
        			printf("\nNumber of trailing set bits: %d \n", cnt_trailing_set_bits(num));
        			printf("\nNumber of trailing clear bits: %d \n", cnt_trailing_clear_bits(num));
				printf("\n");
				break;

			case 9: printf("\nEnter the number: ");
                                scanf("%d", &num);
				
				printf("\nEnter the value of s and d: ");
				scanf("%d %d", &s, &d);

                                printf("\nNum: %d -> ", num);
                                showbits(num);
                                printf("\n");
				
				int a = 10, b = 20;
        			if (maxmin(a, b) == a) {
                			printf("Maximum: %d \t", a);
                			printf("Minimum: %d \n", b);
        			} else {
                			printf("Maximum: %d \t", b);
                			printf("Minimum: %d \n", a);
        			}
	
        			int clr_r_most_bit = clear_right_most_bit(num);
        			printf("After clear right most bit: %d -> ", clr_r_most_bit);
        			showbits(clr_r_most_bit);
        			printf("\n");

        			int clr_l_most_bit = clear_left_most_bit(num);
        			printf("After clear left most bit: %d -> ", clr_l_most_bit);
        			showbits(clr_l_most_bit);
        			printf("\n");

        			int set_r_most_bit = set_right_most_bit(num);
        			printf("After set right most bit: %d -> ", set_r_most_bit);
        			showbits(set_r_most_bit);
        			printf("\n");

        			int set_l_most_bit = set_left_most_bit(num);
        			printf("After set left most bit: %d -> ", set_l_most_bit);
        			showbits(set_l_most_bit);
        			printf("\n");
				
				int store, add;

				SET_BIT_FROM_S_TO_D(num,s,d,add,store);
/*				int set_bit_pos = SET_BIT_FROM_S_TO_D(num,s,d,add,store);
				printf("After setbit from s to d: %d -> ", set_bit_pos);
                                showbits(set_bit_pos);
                               printf("\n");
*/
				CLEAR_BIT_FROM_S_TO_D(num,s,add,store);
/*				int clear_bit_pos = CLEAR_BIT_FROM_S_TO_D(num,s,d,add,store);
                                printf("After clearbit from s to d: %d -> ", clear_bit_pos);
                                showbits(clear_bit_pos);
                                printf("\n"); 
*/
				TOGGLE_BIT_FROM_S_TO_D(num,s,d,add,store);
/*				int toggle_bit_pos = TOGGLE_BIT_FROM_S_TO_D(num,s,d,add,store);
                                printf("After togglebit from s to d: %d -> ", toggle_bit_pos);
                                showbits(toggle_bit_pos);
                                printf("\n");
*/
				break;

			case 10: printf("\nEnter the value of x and y:");
				 scanf("%d%d", &x, &y);

				 printf("\nEnter the value of p: ");
				 scanf("%d", &p);
				 printf("\nEnter the value of n: ");
				 scanf("%d", &n);

				 printf("\nY: %d -> ", y);
        			 showbits(y);
        			 printf("\n");

        			 printf("\nX: %d -> ", x);
        			 showbits(x);
        			 printf("\n");

        			 int s_bits = setbits(x, p, n, y);

        			 printf("\nAfter set bit: %d -> ", s_bits);
        			 showbits(s_bits);
        			 printf("\n\n");

				 break;
				
			case 11: printf("\nEnter the value of x:");
                                 scanf("%d", &x);

                                 printf("\nEnter the value of p: ");
                                 scanf("%d", &p);
                                 printf("\nEnter the value of n: ");
                                 scanf("%d", &n);

				 printf("\nX: %d -> ", x);
        			showbits(x);
        			printf("\n");

        			int invert_bits = invert(x, p, n);

        			printf("\nAfter invert: %d -> ", invert_bits);
        			showbits(invert_bits);
        			printf("\n\n");

				break;

			case 12: printf("\nEnter the value of x:");
                                 scanf("%d", &x);

                                 printf("\nEnter the value of p: ");
                                 scanf("%d", &p);
                                 printf("\nEnter the value of n: ");
                                 scanf("%d", &n);

				printf("\nX: %d -> ", x);
                                showbits(x);
                                printf("\n");

				int bits = get_bits(x, p, n);

        			printf("\nBits: %d -> ", bits);
        			showbits(bits);
        			printf("\n\n");

				break;

			case 0: exit(0);
			default: printf("\nInvalid Choice!!! \n\n ");
		}
	} while(1);

	return 0;
}
