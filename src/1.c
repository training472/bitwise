unsigned int swap_bits(unsigned int num, unsigned int s, unsigned int d)
{
	if (((num & (1 << s)) >> s) ^ ((num & (1 << d)) >> d)) {
		num = num ^ (1 << s);
		num = num ^ (1 << d);
	}

	return num;
}
